import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { filter } from 'rxjs/operators';

export interface Tenant {
  id: string;
  name: string;
}

export interface User {
  displayName: string;
  defaultTenant?: string;
  currentTenant?: string;
  tenants?: Tenant[];
  defaultCdf?: string;
  cdfProjects?: string[];
}

const mockTenantData = {
  'tenant1': {
    displayName: 'Tenant 1 Display Name',
    defaultCdf: 'dev-1-cdf',
    cdfProjects: ['dev-1-cdf', 'qa-1-cdf', 'prod-1-cdf']
  },
  'tenant2': {
    displayName: 'Tenant 2 Display Name',
    defaultCdf: 'dev-2-cdf',
    cdfProjects: ['dev-2-cdf', 'qa-2-cdf', 'prod-2-cdf']
  },
};

const mockUser: User = {
  displayName: '12345',
  defaultTenant: 'tenant1',
  currentTenant: 'tenant1',
  tenants: [
    {
      id: 'tenant1',
      name: 'Tenant 1 Name'
    },
    {
      id: 'tenant2',
      name: 'Tenant 2 Name'
    },
  ],
  defaultCdf: undefined,
  cdfProjects: undefined
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private userProfileSubject$: BehaviorSubject<User | null> = new BehaviorSubject(null);
  userProfile$ = this.userProfileSubject$.asObservable().pipe(filter((user) => !!user));

  private userState = mockUser;

  constructor() {
    this.setCurrentTenant(this.userState.currentTenant);
  }

  public setCurrentTenant(tenantId: string): void {
    this.updateUserProfile(tenantId);
  }

  private async updateUserProfile(tenantId?: string): Promise<any> {
    if (!tenantId) {
      return;
    }

    const tenantData = mockTenantData[tenantId];

    this.userState.currentTenant = tenantId;
    this.userState.displayName = tenantData.displayName;

    this.getCdfInfoForTenant(tenantId);

    this.userProfileSubject$.next(this.userState);
  }

  getCdfInfoForTenant(tenantId: string): void {
    if (!tenantId) {
      return;
    }

    const tenantData = mockTenantData[tenantId];

    setTimeout(() => {
      this.userState.defaultCdf = tenantData.defaultCdf;
      this.userState.cdfProjects = tenantData.cdfProjects;

      this.userProfileSubject$.next(this.userState);
    }, 1000);
  }
}
